from selenium import webdriver
from bs4 import BeautifulSoup
import time
from datetime import datetime
from selenium.webdriver.support import expected_conditions as EC


def moveToCafeArticle(driver, accommodationId, roomId):
    driver.get('https://m.place.naver.com/accommodation/' + accommodationId + '/room/'+ roomId)

def goChoiceDate(driver, roomId):
    time.sleep(1)

    bookingUrl = ''
    soup = BeautifulSoup(driver.page_source, "html.parser")
    for a in soup.find_all('a'):
        if(a['href'].find('https://booking.naver.com/booking') > -1 and a['href'].find(roomId) > -1):
            bookingUrl = a['href']
    
    if(len(bookingUrl) > 0):
        driver.get(bookingUrl)

def diff_month(d1, d2):
    return (d1.year - d2.year) * 12 + d1.month - d2.month

def getCalendarTitle(driver):
    hasCalendar = False
    currentDates = []

    while hasCalendar == False:
        try:
            calendarTitle = driver.find_elements_by_css_selector('div.tit_calendar')
            currentDates = calendarTitle[0].text.split('.')
            hasCalendar = True
            break
        except:
            time.sleep(0.5)
            print('캘린더 메인 제목 못찾음.')
            driver.refresh()
    
    return currentDates

def getDayInfo(day, driver):
    foundDayElement = False
    dateText = ''

    while foundDayElement == False:
        try:
            if(day != None):
                dateText = day.get_attribute('data-tst_cal_datetext')
                foundDayElement = True
                break
        except:
            time.sleep(0.5)
            print('달력 정보 못찾음.')
            driver.refresh()
    
    return dateText
    

def choiceDate(driver, fromDate, toDate, startUrl):
    isFromDateChoice = False
    isToDateChoice = False

    while isFromDateChoice == False or isToDateChoice == False:
        time.sleep(1)

        currentDates = getCalendarTitle(driver)
        
        nextMonthBtn = driver.find_element_by_xpath('//*[@id="calendar"]/div/a[2]')

        # '2021.3'
        fromDates = fromDate.split('-')
        toDates = toDate.split('-')

        fromYear = fromDates[0]
        fromMonth = fromDates[1]
        fromDay = fromDates[2]

        toYear = toDates[0]
        toMonth = toDates[1]
        toDay = toDates[2]

        currentYear = currentDates[0]
        currentMonth = currentDates[1]

        
        # 시작날짜 선택
        diffMonthWithFromDate = diff_month(datetime(int(fromYear),int(fromMonth),int(fromDay)), datetime(int(currentYear),int(currentMonth), 1))
        if(diffMonthWithFromDate > 0):
            for i in range(diffMonthWithFromDate):
                nextMonthBtn.click()

        currentYear = fromYear
        currentMonth = fromMonth

        days = driver.find_elements_by_css_selector('table.tb_calendar td')


        for day in days:
            dateText = getDayInfo(day, driver)
            unselectableDate = day.get_attribute('class').find('calendar-unselectable')

            if(isFromDateChoice == False and fromDate == dateText and unselectableDate == -1):
                day.click()
                isFromDateChoice = True
            if(isFromDateChoice == True and int(currentMonth) == int(toMonth) and toDate == dateText and unselectableDate == -1):
                day.click()
                isToDateChoice = True
                break

        if(isFromDateChoice == True and isToDateChoice == False and int(currentMonth) != int(toMonth)):
            # 종료날짜 선택
            diffMonthWithToDate = diff_month(datetime(int(toYear),int(toMonth),int(toDay)), datetime(int(fromYear),int(fromMonth), int(fromDay)))
            if(diffMonthWithToDate > 0):
                for i in range(diffMonthWithToDate):
                    nextMonthBtn.click()

            days = driver.find_elements_by_css_selector('table.tb_calendar td')
            for day in days:
                if(toDate == day.get_attribute('data-tst_cal_datetext') and day.get_attribute('class').find('calendar-unselectable') == -1):
                    day.click()
                    isToDateChoice = True
                    break

        if(isFromDateChoice == True and isToDateChoice == True):
            nextBtn = driver.find_element_by_xpath('//*[@id="container"]/bk-accommodation/div/div/div[2]/bk-submit/div/button')
            nextBtn.click()

        if(isFromDateChoice == False or isToDateChoice == False):
            isFromDateChoice = False
            isToDateChoice = False
            driver.refresh()
            

def pay(driver):
    hasNextButton = False
    while hasNextButton == False:
        try:
            driver.find_element_by_xpath('//*[@id="container"]/bk-accommodation/div/div/div[2]/bk-submit/div/button').click()
            hasNextButton = True
        except:
            print('결제하기 버튼 못찾음')

