
def login(driver, id, pw):
    driver.get('https://m.naver.com/aside/')

    driver.implicitly_wait(3)

    driver.find_element_by_xpath('//*[@id="MM_aside_shortcut"]/div[1]/a').click()

    driver.implicitly_wait(2)
    
    driver.execute_script("document.getElementsByName('id')[0].value = \'" + id + "\'")
    
    driver.execute_script("document.getElementsByName('pw')[0].value=\'" + pw + "\'")
    
    driver.find_element_by_xpath('//*[@id="log.login"]').click()

    return True