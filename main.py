import tkinter as tk
import tkinter.scrolledtext as st
from selenium import webdriver
import naverLogin
import util
import os
import sys


def login():
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument("--incognito")
    driver = webdriver.Chrome(options=chrome_options, executable_path="chromedriver")

    ok = naverLogin.login(driver, txtNaverId.get(), txtNaverPW.get())
    if(ok):
        startReserve(driver)

def startReserve(driver):
    util.moveToCafeArticle(driver, txtAccommodationId.get(), txtRoomId.get())
    util.goChoiceDate(driver, txtRoomId.get())

    try:
        util.choiceDate(driver, txtFromDate.get(), txtToDate.get(), driver.current_url)
    except:
        print('예약 에러~~~')
        driver.refresh()
        startReserve(driver)
        

    util.pay(driver)

window = tk.Tk()

window.title = "네이버 카페 댓글 등록"
window.geometry("840x400+100+100")


lblNaverId = tk.Label(window, text="네이버 아이디")
lblNaverId.pack()
lblNaverId.grid(row = 0, column=0)

lblNaverPW = tk.Label(window, text="네이버 비밀번호")
lblNaverPW.pack()
lblNaverPW.grid(row = 1, column=0)

txtNaverId = tk.Entry(window, width=20)
txtNaverId.pack()
txtNaverId.grid(row = 0, column = 1)

txtNaverPW = tk.Entry(window, width=20)
txtNaverPW.pack()
txtNaverPW.grid(row = 1, column = 1)

btnLogin = tk.Button(window, text="로그인", command = login)
btnLogin.pack()
btnLogin.grid(row = 4, column = 0)

lblAccommodationId = tk.Label(window, text="accommodationId")
lblAccommodationId.pack()
lblAccommodationId.grid(row = 0, column=4)

txtAccommodationId = tk.Entry(window, width=20)
txtAccommodationId.pack()
txtAccommodationId.grid(row = 0, column = 5)

lblRoomId = tk.Label(window, text="RoomId")
lblRoomId.pack()
lblRoomId.grid(row = 1, column=4)

txtRoomId = tk.Entry(window, width=20)
txtRoomId.pack()
txtRoomId.grid(row = 1, column = 5)

lblFromDate = tk.Label(window, text="시작일")
lblFromDate.pack()
lblFromDate.grid(row = 2, column=0)

txtFromDate = tk.Entry(window, width=20)
txtFromDate.pack()
txtFromDate.grid(row = 2, column = 1)

lblToDate = tk.Label(window, text="종료일")
lblToDate.pack()
lblToDate.grid(row = 2, column = 4)

txtToDate = tk.Entry(window, width=20)
txtToDate.pack()
txtToDate.grid(row = 2, column = 5)




window.mainloop()

def main():
    print('')


if __name__ == '__main__':
    main()

